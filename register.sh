#!/bin/sh

export PASSWORD='changeme'
export HOST_IP=$(ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
echo "$HOST_IP central_ip" >> /etc/hosts

# Updates and installs
aptitude update
aptitude -y upgrade
aptitude -y install kamailio nginx asterisk vim postgresql kamailio-postgres-modules git vim python-pip python-dev python-psycopg2

# Asterisk
sed -i s/'udpbindaddr=0.0.0.0'/'udpbindaddr=0.0.0.0:5080'/g /etc/asterisk/sip.conf
cp  configs/exten_ivr.conf  /etc/asterisk
echo '#include  "exten_ivr.conf"'  >>  /etc/asterisk/extensions.conf
cp configs/peer_kamailio.conf  /etc/asterisk/
echo '#include "peer_kamailio.conf"' >>  /etc/asterisk/sip.conf
cp -a sounds/boxtub/ /usr/share/asterisk/sounds/
chown -R asterisk:asterisk /usr/share/asterisk/sounds/
mkdir /etc/asterisk/keys
sh ./ast_tls_cert -C $HOST_IP -O "My Company" -d /etc/asterisk/keys
chown -R asterisk:asterisk /etc/asterisk/keys/

/etc/init.d/asterisk restart


# App
cp -a app /home/
echo "
[db]
database = kamailio
host = localhost
port = 5432
user = kamailio
password = $PASSWORD
adapter = postgresql

[site]
domain = $HOST_IP
ws = wss://set_domain_or_ip:10443
"> /home/app/config.ini

pip install -r /home/app/requirements.txt
cp -a web /var/www/html/web

# Nginx
cp configs/site_default_nginx /etc/nginx/sites-available/default 

# Kamailio config
cp  configs/kamailio.cfg /etc/kamailio/kamailio.cfg
sed -i s/'#RUN_KAMAILIO=yes'/'RUN_KAMAILIO=yes'/g /etc/default/kamailio
sed -i s/'# DBENGINE=MYSQL'/' DBENGINE=PGSQL'/g /etc/kamailio/kamctlrc
sed -i s/'# DBHOST=localhost'/' DBHOST=localhost'/g /etc/kamailio/kamctlrc
sed -i s/'# DBNAME=kamailio'/' DBNAME=kamailio'/g /etc/kamailio/kamctlrc
sed -i s/'# DBRWUSER="kamailio"'/' DBRWUSER="kamailio"'/g /etc/kamailio/kamctlrc
sed -i s/'# DBRWPW="kamailiorw"'/' DBRWPW="$PASSWORD"'/g /etc/kamailio/kamctlrc
sed -i s/'# DBROUSER="kamailioro"'/' DBROUSER="kamailio"'/g /etc/kamailio/kamctlrc
sed -i s/'# DBROPW="kamailioro"'/' DBROPW="kamailio"'/g /etc/kamailio/kamctlrc
sed -i s/'# DBACCESSHOST=192.168.0.1'/' DBACCESSHOST=localhost'/g /etc/kamailio/kamctlrc
sed -i s/'DBURL "mysql:\/\/kamailio:kamailiorw@localhost\/kamailio"'/'DBURL "postgres:\/\/kamailio:$PASSWORD@localhost\/kamailio"'/g /etc/kamailio/kamailio.cfg



su postgres <<'EOF' 
psql -c "ALTER USER postgres PASSWORD '$PASSWORD';"
psql -c "CREATE USER kamailio WITH PASSWORD '$PASSWORD';"
psql -c "CREATE USER kamailio WITH PASSWORD '$PASSWORD';"
psql -c "CREATE DATABASE kamailio OWNER kamailio;"
EOF
echo 'localhost:*:*:kamailio:changeme' >> ~/.pgpass
echo 'localhost:*:*:postgres:changeme' >> ~/.pgpass
chmod 600 ~/.pgpass
kamdbctl reinit kamailio


/etc/init.d/kamailio restart






echo "Ready :) happy test-demo"
