import os
import sys
import uuid
import hashlib
import ConfigParser
import model

from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import exc
from sqlalchemy.orm.exc import NoResultFound

import random
import itertools

# Config db
dirname, filename = os.path.split(os.path.abspath(__file__))
cfg_file = 'config.ini'
cfg = ConfigParser.ConfigParser()
cfg.read(os.path.join(dirname, cfg_file))

# Session db
Session = sessionmaker(bind=model.engine)
session = Session()



def new_subscriber():
    subscriber_id = str(uuid.uuid4())
    subscriber_pass = str(uuid.uuid4())[:25]
    domain = cfg.get('site', 'domain')
    ha1  = hashlib.md5(subscriber_id + ':' +  domain + ':' + subscriber_pass).hexdigest()
    ha1b = hashlib.md5(subscriber_id + '@' +  domain + ':' + domain + ':' + subscriber_pass).hexdigest()

    subscriber = model.Subscriber(
      username = subscriber_id,
      password = subscriber_pass,
      domain = domain,
      email_address = '',
      ha1 = ha1,
      ha1b = ha1b
    )
    session.add(subscriber)
    session.commit()
    return subscriber


