import os
import sys
import ConfigParser

from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relation
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm import relationship, backref

from datetime import datetime

# Config db
dirname, filename = os.path.split(os.path.abspath(__file__))
cfg_file = 'config.ini'
cfg = ConfigParser.ConfigParser()
cfg.read(os.path.join(dirname, cfg_file))


#FIXME: Please apply other way to do this, is really ugly
engine = create_engine ('%s://%s:%s@%s:%s/%s' \
                        % (cfg.get('db', 'adapter'),
                           cfg.get('db', 'user'),
                           cfg.get('db', 'password'),
                           cfg.get('db', 'host'),
                           cfg.get('db', 'port'),
                           cfg.get('db', 'database')
                        ), echo=False)

DeclarativeBase = declarative_base()
metadata = DeclarativeBase.metadata
metadata.bind = engine
S = sessionmaker(bind=engine)
s = S()

try:
    from sqlalchemy.dialects.postgresql import *
except ImportError:
    from sqlalchemy.databases.postgres import *

# Defines tables
subscriber = Table(u'subscriber', metadata,
    Column('id', INTEGER(), primary_key=True, nullable=False),
    Column('username', TEXT()),
    Column('domain', TEXT()),
    Column('password', TEXT()),
    Column('email_address', TEXT()),
    Column('ha1', TEXT()),
    Column('ha1b', TEXT()),
    Column('rpid', TEXT()),
)

# Class
class Subscriber(DeclarativeBase):
    __table__ = subscriber
    #relation definitions

    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return { 'username' : self.username}

