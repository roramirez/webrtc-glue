#!/usr/bin/python
import os
import sys
import uuid
from flask import Flask, request, jsonify
from flask import render_template, redirect, url_for, send_from_directory
import logging
from logging.handlers import RotatingFileHandler
import database
import model
from werkzeug.exceptions import abort
import ConfigParser
from flask.ext.cors import CORS

# Config db
dirname, filename = os.path.split(os.path.abspath(__file__))
cfg_file = 'config.ini'
cfg = ConfigParser.ConfigParser()
cfg.read(os.path.join(dirname, cfg_file))


# Init flask app
app = Flask(__name__)
CORS(app)
@app.route("/new_subscriber")
def new_subscriber():
    subscriber = database.new_subscriber()
    ws = cfg.get('site', 'ws')

    return jsonify(
      uri="%s@%s" % (subscriber.username, subscriber.domain),
      password=subscriber.password,
      ws=ws)


if __name__ == "__main__":
    # App
    app.debug = True

    from werkzeug.debug import DebuggedApplication
    app.wsgi_app = DebuggedApplication( app.wsgi_app, True )

    app.run(host='0.0.0.0')
