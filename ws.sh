#!/bin/sh

# Updates :)
aptitude update
aptitude -y upgrade


# libs
aptitude -y install git debhelper  libssl-dev libconfuse   libconfuse-dev  autoconf libncap-dev libpcap-dev libncurses5-dev ruby ruby-dev libev-dev vim


# Install OverSIP
git clone  https://github.com/versatica/OverSIP.git
cd OverSIP
sh create-deb.sh
dpkg -i ../oversip_*_all.deb
cd ..
sed -i s/'RUN=no'/'RUN=yes'/g /etc/default/oversip
/etc/init.d/oversip restart

echo "Ready :) happy test-demo"
