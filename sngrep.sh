aptitude install libncurses5-dev libpcap-dev libncursesw5-dev autoconf
cd /usr/src
git clone https://github.com/irontec/sngrep.git
cd sngrep
./bootstrap.sh
./configure
make
make install
